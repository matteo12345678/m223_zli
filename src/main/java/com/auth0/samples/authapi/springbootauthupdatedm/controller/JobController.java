package com.auth0.samples.authapi.springbootauthupdatedm.controller;

import com.auth0.samples.authapi.springbootauthupdatedm.domain.Entry;
import com.auth0.samples.authapi.springbootauthupdatedm.domain.Job;
import com.auth0.samples.authapi.springbootauthupdatedm.repository.JobRepository;
import com.auth0.samples.authapi.springbootauthupdatedm.service.EntryService;
import com.auth0.samples.authapi.springbootauthupdatedm.task.Task;
import com.auth0.samples.authapi.springbootauthupdatedm.task.TaskRepository;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
/**
 * @author: Matteo Samadelli
 * @datum: 10.07.2020
 */
import java.util.List;

@RestController
@RequestMapping("/jobs")
public class JobController {

    private JobRepository jobRepository;

    public JobController(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    @PostMapping
    public void addJob(@RequestBody Job job) {
        jobRepository.save(job);
    }

    @GetMapping
    public List<Job> getJobs() {
        return jobRepository.findAll();
    }

    @PutMapping("/{id}")
    public void editTask(@PathVariable long id, @RequestBody Job job) {
        Job existingJob = jobRepository.findById(id).get();
        Assert.notNull(existingJob, "Task not found");
        existingJob.setDescription(job.getDescription());
        jobRepository.save(existingJob);
    }

    @DeleteMapping("/{id}")
    public void deleteJob(@PathVariable long id) {
        Job jobToDel = jobRepository.findById(id).get();
        jobRepository.delete(jobToDel);
    }
}
