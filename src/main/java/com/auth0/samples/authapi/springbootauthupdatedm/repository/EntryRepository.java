package com.auth0.samples.authapi.springbootauthupdatedm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.auth0.samples.authapi.springbootauthupdatedm.domain.Entry;

public interface EntryRepository extends JpaRepository<Entry, Long> {
}
