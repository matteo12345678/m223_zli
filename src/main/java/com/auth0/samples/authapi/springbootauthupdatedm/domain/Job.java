package com.auth0.samples.authapi.springbootauthupdatedm.domain;

import com.auth0.samples.authapi.springbootauthupdatedm.user.ApplicationUser;



import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
public class Job {

    //@OneToMany(mappedBy = "Job")
    //private List<Entry> entries;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;



    protected Job() {}

    public Job(String name, String description) {
        super();
        this.name = name;
        this. description = description;
    }

    public Long getId() { return id; }

    //public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}
