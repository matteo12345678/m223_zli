package com.auth0.samples.authapi.springbootauthupdatedm.user;

import com.auth0.samples.authapi.springbootauthupdatedm.domain.Job;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;

@Entity
public class ApplicationUser {

    //@ManyToOne
    //@JoinColumn(name = "job_id", referencedColumnName = "id")
    //private Job job;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String username;
    private String password;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
