package com.auth0.samples.authapi.springbootauthupdatedm.repository;

import com.auth0.samples.authapi.springbootauthupdatedm.domain.Job;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository  extends JpaRepository<Job, Long> {
}
