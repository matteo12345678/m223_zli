/**
 * @author: Matteo Samadelli
 * @datum: 10.07.2020
 */

package com.auth0.samples.authapi.springbootauthupdatedm;

import com.auth0.samples.authapi.springbootauthupdatedm.domain.Job;
import com.auth0.samples.authapi.springbootauthupdatedm.repository.JobRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.auth0.samples.authapi.springbootauthupdatedm.repository.EntryRepository;
import com.auth0.samples.authapi.springbootauthupdatedm.domain.Entry;

import java.time.LocalDateTime;

@SpringBootApplication
public class SpringbootAuthUpdatedMApplication {

	@Bean
	//private static final Logger log = LoggerFactory.getLogger(SpringbootAuthUpdatedMApplication.class);
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	public static void main(String[] args) {
		SpringApplication.run(SpringbootAuthUpdatedMApplication.class, args);
	}


	/*
	@Bean
	public CommandLineRunner demo(JobRepository repository) {
		return (args) -> {
			// save a couple of customers
			Job e1 = new Job("Programmer", "Code schreiben, warten");
			Job e2 = new Job("Mechaniker", "Maschienen bauen");
			Job e3 = new Job("Pilot", "Flugzeuge fliegen");

			repository.save(e1);
			repository.save(e2);
			repository.save(e3);

			// fetch all customers
			log.info("Entries found with findAll():");
			log.info("-------------------------------");
			for (Job job : repository.findAll()) {
				log.info(job.toString());
			}
			log.info("");


		};
	} */
}
