package com.auth0.samples.authapi.springbootauthupdatedm.task;


import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}