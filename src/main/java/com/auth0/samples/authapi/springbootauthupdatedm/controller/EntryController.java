package com.auth0.samples.authapi.springbootauthupdatedm.controller;


import com.auth0.samples.authapi.springbootauthupdatedm.domain.Job;
import com.auth0.samples.authapi.springbootauthupdatedm.repository.JobRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.auth0.samples.authapi.springbootauthupdatedm.domain.Entry;
import com.auth0.samples.authapi.springbootauthupdatedm.service.EntryService;
import com.auth0.samples.authapi.springbootauthupdatedm.repository.EntryRepository;

import javax.ws.rs.core.Response;
import java.util.List;

// https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/ResponseStatus.html
// https://www.baeldung.com/spring-response-status

@RestController
@RequestMapping("/entries")
public class EntryController {


    /*
    private EntryService entryService;
    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    } */

    private EntryRepository entryRepository;

    public EntryController(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }


    
    /* @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Entry> getAllEntries() {
        return entryService.findAllEntries();
    } */

    @GetMapping
    public List<Entry> getEntries() {
        return entryRepository.findAll();
    }

    @PostMapping
    public void addEntry(@RequestBody Entry entry) {
        entryRepository.save(entry);
    }

    /*@GetMapping("/allresponses")
//    @Produces({ "application/json" })
    public Response getAllEntriesResponse() {
		return Response.status(200)
				.entity(entryService.findAllEntries())
				.build();
	}*/
    
   // ... 
    
    //.. 
    
    
    

}
